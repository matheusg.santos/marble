Funcionalidade: Página Inicial

    Cenário: Ver os últimos títulos publicados
        Dado abro o site
        Quando a página inicial for apresentada
        Então quero ver uma lista dos últimos títulos publicados
